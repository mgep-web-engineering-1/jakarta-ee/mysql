# MySQL

Proiektu honek, ```mvc_exercise``` ariketa funtzionarazteko, MySQL martxan jartzeko pausuak azalduko dizkizu.

## Docker-Compose erabiliz

2 bolumen muntatuko ditugu:

1. ```mvc_exercise.sql``` fitxategia kargatzeko datu basea sortzeko edukiontzia (container) martxan jartzen denean.
1. ```volume``` karpeta muntatuko duena datu baseko datuak gordetzeko. Horrela, edukiontzia (container) ezabatzen badugu ere, datu basean egindako aldaketak ez ditugu galduko.

Nola jarri martxan (docker eta docker-compose instalatuta egon behar dira aurretik):

```bash
docker-compose up -d
```

Geldiarazteko:

```bash
docker-compose down
```

## Docker gabe

Ez baduzu docker erabili nahi edo aurretik MySQL martxan baduzu, MySQL bezeroan sartu eta hau exekutatu behar duzu:

```sql
DROP DATABASE IF EXISTS mvc_exercise;
CREATE DATABASE mvc_exercise;

CREATE USER 'admin'@'%'
  IDENTIFIED BY 'admin@eskola';
GRANT ALL
  ON mvc_exercise.*
  TO 'admin'@'%';
```

Ondoren, ```mvc_exercise.sql``` fitxategian dagoen edukia kopiatu eta hori ere exekutatu.
